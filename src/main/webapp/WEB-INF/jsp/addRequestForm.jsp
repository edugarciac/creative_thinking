<%@include file="include.jsp"%>
<fmt:setLocale value="<%=request.getLocale()%>" />
<fmt:setBundle basename="Language-ext" />

<div class="creationRequestPortlet">
<form name="<portlet:namespace/>addRequestForm" method="post" action='<portlet:actionURL name="addRequestAction"/>' enctype="application/x-www-form-urlencoded">
<table>
	<tr>
		<td><a class="anchor" href='<portlet:renderURL portletMode="view"/>'><b><fmt:message key="label.back" /></b><br><br></a></td>
	</tr>
</table>



<table>
	<tr>
		<td><b><fmt:message key="label.request.title" /></b><font style="color: #C11B17;">*</font></td>
		<td><input type="text" name="<portlet:namespace/>title"
			value='<c:out value="${requestScope.request.title}"/>' size="100" maxlength="100"/></td>
		<td>
		<font style="color: #C11B17;">
		<c:if test="${not empty requestScope.errors.title}">
			<fmt:message key="${requestScope.errors.title}" />
		</c:if>
		</font></td>
	</tr>
	<tr>
		<td><b><fmt:message key="label.request.description" /></b><font style="color: #C11B17;">*</font></td>
		<td><textarea cols="80" rows="5" name="<portlet:namespace/>description"
			value='<c:out value="${requestScope.request.description}"/>'></textarea></td>
		<td><font style="color: #C11B17;">
		<c:if test="${not empty requestScope.errors.description}">
			<fmt:message key="${requestScope.errors.description}" />
		</c:if>
		</font></td>
	</tr>

	<tr align="center">
		<td colspan="2"><input type="submit" value='<fmt:message key="label.add.request" />'/></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>
</form>
</div>
<br></br>
<br></br>