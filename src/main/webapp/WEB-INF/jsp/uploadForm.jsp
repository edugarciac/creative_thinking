<%@include file="include.jsp"%>
<fmt:setLocale value="<%=request.getLocale()%>" />
<fmt:setBundle basename="Language-ext" />

<div class="CreativePortlet">
<form name="<portlet:namespace/>uploadTocForm" method="post" action='<portlet:actionURL name="uploadTocAction" copyCurrentRenderParameters="true"/>' enctype="multipart/form-data">
<table>
	<tr>
		<a class="anchor" href='<portlet:renderURL portletMode="view"/>'><b><fmt:message key="label.back" /></b><br><br></a>
	</tr>
</table>
<table>
	<tr>
	<td colspan="3"><fmt:message key="message.upload" /></td>
	</tr>
	<br><br><br><br>
	<tr>
		<td><b><fmt:message key="label.file" /></b><font style="color: #C11B17;">&nbsp; </font></td>
		<td colspan="2"><input type="file" name="<portlet:namespace/>requestFile" /><br><br></td>
		<td><font style="color: #C11B17;">
		<c:if test="${not empty requestScope.errors.file}">
			<fmt:message key="${requestScope.errors.file}" />
		</c:if>
		</font></td>
	</tr>
	<tr align="center">
		<td colspan="3"><input type="submit" value="Upload" /></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>
</form>
</div>
<br></br>
<br></br>