<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page contentType="text/html" isELIgnored="false"%>

<fmt:setLocale value="<%=request.getLocale()%>" />
<fmt:setBundle basename="Language-ext" />
<div class="CreativePortlet">
<form name="<portlet:namespace/>uploadTocForm" method="post" action='<portlet:actionURL name="uploadTocAction" copyCurrentRenderParameters="true"/>' enctype="multipart/form-data">
<table>
	<tr>
		<a class="anchor" href='<portlet:renderURL portletMode="view"/>'><b><fmt:message key="label.back" /></b><br><br></a>
	</tr>
</table>
<table>
	<tr>
		<td><b><fmt:message key="label.file" />:</b>&nbsp;  ${file.name}</td>	
	</tr>
	<tr>
		<td><b><fmt:message key="label.extension" />:</b>&nbsp;  ${file.extension}</td>	
	<tr>
		<td><img id="ItemPreview" src="data:image/${file.extension};base64,<%=request.getAttribute("bytes")%>" /></td>
	</tr>

	<tr>
		<td>&nbsp;</td>
	</tr>
</table>
</form>
</div>
<br></br>
<br></br>