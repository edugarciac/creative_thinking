<%@include file="include.jsp"%>
<fmt:setLocale value="<%=request.getLocale()%>" />
<fmt:setBundle basename="Language-ext" />
<portlet:defineObjects/>

<c:if test="${not empty requests}">
	<table border="1">
		<tr bgcolor="#99CCFF">
			<td valign="top"><b><fmt:message key="label.request.title" /></b></td>
			<td valign="top"><b><fmt:message key="label.request.description" /></b></td>
		</tr>
		<c:forEach var="request" items="${requests}">
			<tr>
				<td valign="top"><c:out value="${request.title}" /></td>
				<td valign="top"><c:out value="${request.description}" /></td>
		</c:forEach>
	</table>
</c:if> <br></br>
<br></br>
<br></br>