<%@include file="include.jsp"%>
<fmt:setLocale value="<%=request.getLocale()%>" />
<fmt:setBundle basename="Language-ext" />
<portlet:defineObjects /> 
<script type='text/javascript'>
  function <portlet:namespace/>confirmRemove() {
    return confirm('<fmt:message key="message.confirm.remove" />');
  }
</script>
<div class="CreativePortlet">
<%
	PortletURL printModeUrl = renderResponse.createRenderURL();
	if (renderRequest.isPortletModeAllowed(new PortletMode("print"))) {
		printModeUrl.setPortletMode(new PortletMode("print"));
	}
	if (renderRequest.isWindowStateAllowed(new WindowState("pop_up"))) {
		printModeUrl.setWindowState(new WindowState("pop_up"));
	}
	PortletURL fullScreenUrl = renderResponse.createRenderURL();
	if(renderRequest.isWindowStateAllowed(WindowState.MAXIMIZED)) {
    	fullScreenUrl.setWindowState(WindowState.MAXIMIZED);
	}
%>
<table align="right">
	<tr>
		<td><b><fmt:message key="label.hello" /></b></td>
		<td>&nbsp; &nbsp; <i><b><c:out
			value="${requestScope.firstName}" /></b></i></td>
		<td>&nbsp; <i><b><c:out value="${requestScope.lastName}" /></b></i></td>
	</tr>
</table>
<br/>
<table align="left">
	<tr>
		<td><a class="anchor" href="<%= printModeUrl%>"><b>&nbsp;<fmt:message key="label.print" />&nbsp;</b></a></td>
		<td><a class="anchor"
			href='<portlet:renderURL portletMode="help"/>'><b>&nbsp;<fmt:message
			key="label.help" />&nbsp;</b></a></td>
		<td><a class="anchor"
			href='<portlet:renderURL portletMode="edit"/>'><b>&nbsp;<fmt:message
			key="label.preferences" />&nbsp;</b></a></td>
		<td><b><a class="anchor"
			href='<portlet:actionURL name="resetAction"/>'> <c:if
			test="${myaction eq 'showSearchResults'}">
			<fmt:message key="label.reset" />
		</c:if></a> </b></td>
	</tr>
</table>
<br/>
<table align="right">
	<tr>
		<td><a class="anchor" href="<%=fullScreenUrl%>"><b><fmt:message
			key="label.fullScreen" /></b></td>
	</tr>
</table>
<table aligh="left">
	<tr>
		<td><br><br><fmt:message key="intro.home" /><br><br></td>
	</tr>
</table>


<!-- form name="<portlet:namespace/>searchForm" method="post" action='<portlet:actionURL name="searchRequestAction"></portlet:actionURL>'>
<table border="0" align="left">
	<tr>
		<td><b><fmt:message key="label.request.title" /></b></td>
		<td>
		<input type="text" name="<portlet:namespace/>requestNameSearchField" value=""/>
		</td>
	</tr>
	<tr>
		<td><b><fmt:message key="label.author.name" /></b></td>
		<td><input type="text" name="<portlet:namespace/>authorNameSearchField" value="" /></td>
	</tr>
	<tr align="center">
		<td colspan="2"><input type="submit"
			value='<fmt:message key="label.search.button"/>'></td>
	</tr>
</table>
</form-->

<form name="<portlet:namespace/>requestForm" method="post"
	action='<portlet:renderURL><portlet:param name="<%=Constants.MYACTION_PARAM%>" value="addRequestForm"/> 
    </portlet:renderURL>'>
<br/>

<c:if test="${not empty requests}">
	<table border="1" width="100%">
		<tr bgcolor="#99CCFF">
			<td valign="top" width="20%"><b><fmt:message key="label.request.title" /></b></td>
			<td valign="top"><b><fmt:message key="label.request.description" /></b></td>
			<td valign="top" width="15%"><b><fmt:message key="label.createdDate" /></b></td>
			<td valign="top" width="10%"><b><fmt:message key="label.file" /></b></td>
			<td valign="top" width="8%"><b><fmt:message key="label.action" /></b></td>
		</tr>
		<c:forEach var="request" items="${requests}">
			<tr>
			<td valign="top"><c:out value="${request.title}" /></td>
			<td valign="top"><c:out value="${request.description}" /></td>
			<td valign="top"><fmt:formatDate value="${request.createdDate}" pattern="MM-dd-yyyy HH:mm:ss"/></td>
			<td valign="top"><a class="anchor" href='
				<portlet:renderURL>
					<portlet:param name="<%=Constants.MYACTION_PARAM%>" value="downloadTocForm" />
					<portlet:param name="requestId" value="${request.requestId}" />
				</portlet:renderURL>
				'><fmt:message key="label.download" /></a>/
				<c:choose>
					<c:when test='${empty requestScope.firstName}'>
						<fmt:message key="message.upload.disabled" />
					</c:when>
					<c:otherwise>
						<a class="anchor" href='
							<portlet:renderURL>
								<portlet:param name="<%=Constants.MYACTION_PARAM%>" value="uploadTocForm" />
								<portlet:param name="requestId" value="${request.requestId}" />
							</portlet:renderURL>
						'>
						<fmt:message key="label.upload" /></a>&nbsp;&nbsp;</b>
					</c:otherwise>
				</c:choose>
			</td>
			<c:choose>
				<c:when test='${empty requestScope.firstName}'>
					<td></td>
				</c:when>
				<c:otherwise>
				<td align="center" valign="top" width="100px"><a class="anchor"
				href='
						<portlet:actionURL name="removeRequestAction">
							<portlet:param name="requestId" value="${request.requestId}" />
						</portlet:actionURL>					
					'
				onclick="javascript: return <portlet:namespace/>confirmRemove()"><b><fmt:message key="label.remove"/></b></a></td>
				</c:otherwise>
			</c:choose>
			</tr>
		</c:forEach>
	</table>
</c:if> <br></br>

<table align="right">
	<tr>
	<c:choose>
	<c:when test='${empty requestScope.firstName}'>
		<td><fmt:message key="label.message.userlogin" /></td>
	</c:when>
	<c:otherwise>
		<td><input type="submit" value='<fmt:message key="label.add.request" />'/></td>
	</c:otherwise>
	</c:choose>
	</tr>
</table>
</form>
</div>
<br></br>