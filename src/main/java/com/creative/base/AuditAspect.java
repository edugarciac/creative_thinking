package com.creative.base;

import java.util.Date;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

import com.creative.domain.CreationRequest;

@Aspect
public class AuditAspect {

	@Before("execution(* com.creative.service.CreationRequestService.addRequest(..)) && args(request,..)")
	public void before(CreationRequest request) {
		if(request.getCreatedDate() == null) {
			request.setCreatedDate(new Date());
		} else {
			request.setModifiedDate(new Date());
		}
	}
}
