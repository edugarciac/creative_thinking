package com.creative.base;

public class RequestAlreadyExistsException extends Exception {

	private static final long serialVersionUID = 4031346128258698247L;
	
	public RequestAlreadyExistsException(String message) {
		super(message);
	}
}
