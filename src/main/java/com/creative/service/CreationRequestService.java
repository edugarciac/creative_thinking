package com.creative.service;

import java.util.List;

import com.creative.base.RequestAlreadyExistsException;
import com.creative.base.UserNotLoggedInException;
import com.creative.domain.CreationRequest;
import com.creative.domain.CreationRequestFile;

public interface CreationRequestService {
	
	List<CreationRequest> getRequests();
	void addRequest(CreationRequest request, String user) throws RequestAlreadyExistsException, UserNotLoggedInException;
	boolean isUniqueId(int id);
	CreationRequest getRequest(int id);
	void removeRequest(int id);
	List<CreationRequest> searchRequests(String requestTitle, String description);
	void uploadRequestFile(CreationRequestFile requestFile);
	CreationRequestFile getRequestFile(int idFile);
	
	
}
