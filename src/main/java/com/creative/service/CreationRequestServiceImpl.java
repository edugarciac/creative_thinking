package com.creative.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.creative.base.RequestAlreadyExistsException;
import com.creative.base.UserNotLoggedInException;
import com.creative.dao.RequestDao;
import com.creative.dao.RequestFileDao;
import com.creative.domain.CreationRequest;
import com.creative.domain.CreationRequestFile;

@Service("requestService")
public class CreationRequestServiceImpl implements CreationRequestService {
	@Autowired
	@Qualifier("requestDao")
	private RequestDao requestDao;
	
	@Autowired
	@Qualifier("requestFileDao")
	private RequestFileDao requestFileDao;
	
	@Transactional
	public List<CreationRequest> getRequests() {
		return requestDao.getRequests();
	}
	
	@Transactional
	public List<CreationRequest> searchRequests(String title, String description) {
		return requestDao.searchRequests(title, description);
	}
	
	@Transactional
	public void addRequest(CreationRequest request, String user) throws RequestAlreadyExistsException, UserNotLoggedInException {
		if(user == null || "".equals(user)) {
			throw new UserNotLoggedInException("Si us plau fes login per poder afegir peticions");
		}
		if(requestDao.isUniqueRequest(request.getRequestId())) { 
			request.setCreatedBy(user);
			requestDao.addRequest(request);
		} else {
			throw new RequestAlreadyExistsException("Petició amb id : " + request.getRequestId() + " ja existeix");
		}
	}
	
	@Transactional
	public void removeRequest(int requestId) {
		requestDao.removeRequest(requestId);
		requestFileDao.removeRequestFile(requestId);
	}
	
	@Transactional
	public boolean isUniqueId(int requestId) {
		return requestDao.isUniqueRequest(requestId);
	}

	@Transactional
	public CreationRequest getRequest(int requestId) {
		return requestDao.getRequest(requestId);
	}
	
	@Transactional
	public void uploadRequestFile(CreationRequestFile requestFile) {
		requestFileDao.removeRequestFile(requestFile.getFileId());
		requestFileDao.uploadRequestFile(requestFile);
	}
	
	@Transactional
	public CreationRequestFile getRequestFile(int idFile) {
		return requestFileDao.getRequestFile(idFile);
	}
}