package com.creative.dao;

import org.hibernate.SessionFactory;
import org.hibernate.mapping.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.creative.domain.CreationRequest;
import com.creative.domain.CreationRequestFile;

@Repository("requestFileDao")
public class HibernateRequestFileDao implements RequestFileDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public void uploadRequestFile(final CreationRequestFile requestFile) {
		requestFile.setActive(1);
		sessionFactory.getCurrentSession().save(requestFile);
	}

	public void removeRequestFile(long fileId) {
		String hql = "from CreationRequestFile as toc where toc.active=1 and requestFile.fileId=" + fileId;
		CreationRequestFile requestFile = (CreationRequestFile)sessionFactory.getCurrentSession().createQuery(hql).uniqueResult();
		if(requestFile != null) {
			requestFile.setActive(0);
			sessionFactory.getCurrentSession().saveOrUpdate(requestFile);
		}
	}

	public CreationRequestFile getRequestFile(int requestId) {
		String hql = "from CreationRequestFile as toc where toc.active=1 and requestFile.requestId=" + requestId;
		java.util.List<CreationRequestFile> list= sessionFactory.getCurrentSession().createQuery(hql).list();
		return (CreationRequestFile) list.get(list.size()-1);
	}
}
