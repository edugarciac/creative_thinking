package com.creative.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.creative.domain.CreationRequest;

@Repository("requestDao")
public class HibernateRequestDao implements RequestDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public List<CreationRequest> getRequests() {
		return sessionFactory.getCurrentSession().createQuery("from MyCreationRequest as request where request.active=1 order by createdDate").list();
	}

	public List<CreationRequest> searchRequests(String title, String description) {
		return null;
	}

	public void addRequest(final CreationRequest request) {
		request.setActive(1);
		sessionFactory.getCurrentSession().save(request);
	}

	public CreationRequest getRequest(int requestId) {
		String hql = "from MyCreationRequest as request where MyCreationRequest.active=1 and request.requestId=" + requestId;
		return (CreationRequest) sessionFactory.getCurrentSession().createQuery(hql).uniqueResult();
	}

	public boolean isUniqueRequest(int requestId) {
		String hql = "from MyCreationRequest as request where request.active=1 and request.requestId=" + requestId;
		CreationRequest request = (CreationRequest)sessionFactory.getCurrentSession().createQuery(hql).uniqueResult();
		if(request == null) {
			return true;
		} else {
			return false;
		}
	}

	public void removeRequest(int requestId) {
		String hql = "from MyCreationRequest as request where request.requestId=" + requestId;
		CreationRequest request = (CreationRequest)sessionFactory.getCurrentSession().createQuery(hql).uniqueResult();
		if(request != null) {
			request.setActive(0);
			sessionFactory.getCurrentSession().save(request);
		}
	}
}