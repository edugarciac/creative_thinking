package com.creative.dao;

import com.creative.domain.CreationRequestFile;

public interface RequestFileDao {
	void uploadRequestFile(CreationRequestFile requestFile);
	void removeRequestFile(long fileId);
	CreationRequestFile getRequestFile(int fileId);
}
