package com.creative.dao;

import java.util.List;

import com.creative.domain.CreationRequest;

public interface RequestDao {
	List<CreationRequest> getRequests();
	List<CreationRequest> searchRequests(String title, String description);
	void addRequest(CreationRequest request);
	void removeRequest(int requestId);
	boolean isUniqueRequest(int requestId);
	CreationRequest getRequest(int requestId);
}
