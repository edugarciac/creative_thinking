package com.creative.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name="MyCreationRequest")
@Table(name="tfc_creationrequest")
public class CreationRequest extends AuditFields {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="requestId")
	private int requestId;

	@Column(name="title")
	private String title;
	
	@Column(name="description")
	private String description;
	
	@Column(name="active")
	private int active;


	
	

	public CreationRequest(String title, String description) {
		this.title = title;
		this.description = description;
	}

	@SuppressWarnings("unused")
	protected CreationRequest() {
		
	}
	
	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public int getRequestId() {
		return requestId;
	}

	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public boolean equals(Object otherObject) {
		CreationRequest otherCreationRequest = (CreationRequest)otherObject;
		if(otherCreationRequest.getRequestId() == this.requestId) {
			return true;
		} else {
			return false;
		}
	}
}