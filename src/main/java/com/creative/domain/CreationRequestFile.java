package com.creative.domain;

import java.util.Arrays;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name="CreationRequestFile")
@Table(name="tfc_creationrequestfile")
public class CreationRequestFile {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="fileId")
	private long fileId;

	@Column(name="requestId")
	private long requestId;

	@Column(name="requestFile")
	private byte[] requestFile;

	@Column(name="name")
	private String name;

	@Column(name="extension")
	private String extension;
	
	@Column(name="active")
	private int active;
	
	public CreationRequestFile() {
		
	}
	
	public CreationRequestFile(long requestId, String name, String extension, byte[] requestFile) {
		this.requestId = requestId;
		this.name = name;
		this.extension = extension;
		this.requestFile = requestFile;
	}

	public long getFileId() {
		return fileId;
	}

	public void setFileId(long fileId) {
		this.fileId = fileId;
	}

	public long getRequestId() {
		return requestId;
	}

	public void setRequestId(long requestId) {
		this.requestId = requestId;
	}

	public byte[] getRequestFile() {
		return requestFile;
	}

	public void setRequestFile(byte[] requestFile) {
		this.requestFile = requestFile;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	@Override
	public String toString() {
		return "CreationRequestFile [fileId=" + fileId + ", requestId="
				+ requestId + ", requestFile=" + Arrays.toString(requestFile)
				+ ", name=" + name + ", extension=" + extension + ", active="
				+ active + "]";
	}
	
	

}
