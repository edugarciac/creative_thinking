package com.creative.utils;

public class Constants {
	//--identified the name of the context attribute which has the Creation requests data
	//-- in a real application this data will be in the database or any other data store
	public static final String CREATION_REQUEST_DATA_ATTR = "creationRequest";
	public static final String PATH_TO_JSP_PAGE = "/WEB-INF/jsp/";
	public static final String MATCHING_REQUESTS_ATTR = "matchingRequests";
	public static final String REQUESTS_ATTR = "requests";
	public static final String FILE_ATTR = "file";
	public static final String MYACTION_PARAM = "myaction";
	public static final String PREF_RESOURCE_IDENTIFIER_PREFIX = "javax.portlet.preference";
	public static final String JETSPEED = "Jetspeed";
	public static final String LIFERAY = "Liferay";
	public static final String GLASSFISH = "Glassfish";
}
