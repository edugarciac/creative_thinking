DROP TABLE IF EXISTS `tfc_creationrequestfile`;
CREATE TABLE `tfc_creationrequestfile` (
  `fileId` int(11) NOT NULL AUTO_INCREMENT,
  `requestId` int(11) NOT NULL,
  `requestFile` mediumblob DEFAULT NULL,  
  `name` varchar(100) DEFAULT NULL,
  `extension` varchar(3) DEFAULT NULL,
  `active` int(1) DEFAULT 1,
  PRIMARY KEY (`fileId`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;

#
#  Foreign keys for tfc_creationrequestfile
#

ALTER TABLE `tfc_creationrequestfile`
ADD CONSTRAINT `tfc_creationrequestfile_fk1` FOREIGN KEY (`requestId`) REFERENCES `tfc_creationrequest` (`requestId`);