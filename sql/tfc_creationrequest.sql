DROP TABLE IF EXISTS `tfc_creationrequest`;
CREATE TABLE `tfc_creationrequest` (
  `requestId` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `description` varchar(250) NOT NULL,
  `createdDate` datetime NOT NULL,
  `createdBy` varchar(100) NOT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `modifiedBy` varchar(100) DEFAULT NULL,
  `active` int(1) DEFAULT 1,
  PRIMARY KEY (`requestId`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;

